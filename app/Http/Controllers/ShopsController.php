<?php

namespace App\Http\Controllers;

use App\Repositories\TodoInterfaceWork\SupportRepositories;
use App\Admin\Shops;
use App\Admin\Adress;
use App\Http\Requests\Shops\AddNewShops;
use Illuminate\Http\Request;

use DB;

class ShopsController extends Controller
{

    private $support;

    public function __construct(SupportRepositories $support){
        $this->support = $support;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shops = Shops::with('Adress')->paginate(5);
        return response()->json($shops);
    }

    public function store(Request $request){
        // shop
        $shop = new Shops();
        if($request->picture_shop)
        {
            $shop->photos = $this->support->SetPicture($request->picture_shop);
        }
        else{
            $shop->photos = '';
        }
        $shop->empl_id = rand(1,10);
        $shop->domain = $request->domain;
        $shop->name = $request->name_shop;
        $shop->description = $request->description;
        $shop->time_start = $this->support->getHours();
        $shop->save();
        $id = $shop->id;
        // address
        $address = new Adress();
        $address->shop_id = $shop->id;
        $address->dis_id = $request->dis_id;
        $address->save();
        return 'done';
    }

    public function show($id){
        $shops = Shops::with('Adress')->where('id_shop',$id)->first();
        return response()->json($shops);
    }

   

   
}
