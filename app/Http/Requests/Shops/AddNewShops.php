<?php

namespace App\Http\Requests\Shops;

use Illuminate\Foundation\Http\FormRequest;

class AddNewShops extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_shop' => 'required|min:3',
            'add_shop' => 'required|min:3',
            'dis_id' => 'required',
            'description' => 'required|min:3',
            'picture_shop' => 'required'
        ];
    }
    public function messages(){
        return [
            'name_shop.required' => 'Tên cửa hàng không được để trống',
            'name_shop.min:3' => 'Không được nhỏ hơn 3 ký tự',
            'add_shop.required' => 'Địa chỉ không được để trống',
            'add_shop.min'=> 'Không được nhỏ hơn 3 ký tự',
            'dis_id.required' => 'Thành phố không được bỏ trống',
            'description.required' => 'Mô tả không được để trống',
            'description.min' => 'Không được nhỏ hơn 3 ký tự',
            'picture_shop.required' => 'Bạn chưa chọn hình ảnh'
        ];
    }
}
