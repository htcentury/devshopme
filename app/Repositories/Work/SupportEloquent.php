<?php


namespace App\Repositories\Work;

use App\Repositories\TodoInterfaceWork\SupportRepositories;
use Image;
use DB;
class SupportEloquent implements SupportRepositories
{

    // get time now with hours
    public function getHours()
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        return date('H:i:s');
    }

    //Set Images
    public function SetPicture($image){
        //Đến số ký tự trong chuỗi photos từ dấu ';' trở ngược về trước
        $strpos= strpos($image, ';');
        //cắt chuỗi trong chuỗi photos từ vị trí 0 đến số lượng của $strpos
        $sub = substr($image, 0, $strpos);
        //Cắt chuỗi thành mảng ngay tại vị trí có dấu '/'
        $ex = explode('/', $sub)[1];
        //Đặt tên xử lý hình ảnh qua hàm time()
        $name = time().'.'.$ex;
        //Tạo size ảnh
        $img = Image::make($image)->resize(200,200);
        //Lưu trữ hình ảnh
        $upload_path = public_path()."/UploadImage/";
        $img->save($upload_path.$name);
        return $name;
    }

    
}
