<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Admin\Adress;
use Faker\Generator as Faker;

$factory->define(Adress::class, function (Faker $faker) {
    return [
        'id_shop' => rand(1,10),
        'id_dis' => rand(1,10),
        'a_name' => $faker->title,
        'sh_name' => $faker->name,
    ];
});
