<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Admin\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'typecate_id'=> rand(1,10),
        'c_name' => $faker->name,
        'c_description' => $faker->text,
    ];
});
