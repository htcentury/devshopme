<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Admin\Products;
use Faker\Generator as Faker;

$factory->define(Products::class, function (Faker $faker) {
    return [
        'p_name' => $faker->name,
        'p_price' => rand(1,100),
        'p_comment' => $faker->title,
        'p_description' => $faker->text,
        'p_picture' => $faker->imageUrl(),
    ];
});
