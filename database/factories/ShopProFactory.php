<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Admin\Shop_pro;
use Faker\Generator as Faker;

$factory->define(Shop_pro::class, function (Faker $faker) {
    return [
        'sh_domain' => $faker->title,
        'empl_id' => $faker->randomNumber(),
        's_display' => $faker->title,
        'del_floag' =>$faker->randomNumber(),
        'name' => $faker->name,
        'photos' => $faker->imageUrl(),
        'sh_description' -> $faker->title,

    ];
});
