<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Admin\Shops;
use Faker\Generator as Faker;

$factory->define(Shops::class, function (Faker $faker) {
    return [
            'empl_id' => rand(1,10),
            'sh_domain' => $faker->url,
            'name' => $faker->name,
            'photos' =>$faker->imageUrl(),
            'sh_validate' => rand(1,10),
            'time_start' => $faker->time(),
            'time_end'=>$faker->time(),
            'sh_description' => $faker->text,
            'del_floag' => rand(1,10)
    ];
});
