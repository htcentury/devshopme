<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->Increments('sh_id');
            $table->integer('empl_id')->unsigned()->nullable();
            $table->string('sh_domain')->nullable();
            $table->string('name')->nullable();
            $table->string('photos')->nullable();
            $table->float('sh_rate_mark')->nullable();
            $table->integer('sh_status')->nullable()->default(1);
            $table->DateTime('sh_active')->nullable();
            $table->integer('sh_validate')->nullable();
            $table->time('time_start')->nullable();
            $table->time('time_end')->nullable();
            $table->text('sh_description')->nullable();
            $table->integer('del_floag')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}
