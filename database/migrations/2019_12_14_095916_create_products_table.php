<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('p_id');
            $table->string('p_name')->nullable();
            $table->float('p_price')->nullable();
            $table->string('p_comment')->nullable();
            $table->text('p_description')->nullable();
            $table->string('p_picture')->nullable();
            $table->integer('p_rateMark')->nullable()->default(5);
            $table->integer('p_totalBought')->default(1);
            $table->integer('del_flag')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
