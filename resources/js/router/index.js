import vue from "vue";
import VueRouter from "vue-router";
import Setting from "../components/setting/index";
import ListShops from "../components/shop/list";
import AddShop from "../components/shop/add";
import EditShop from "../components/shop/edit";
vue.use(VueRouter);

export default new VueRouter({
    routes: [
        {path: '/setting', component:Setting},
        {path: '/list-shop', component: ListShops},
        {path: '/add-shop', component:AddShop},
        {path: '/edit-shop/:id', component:EditShop}
    ],
    mode: 'history'
})
