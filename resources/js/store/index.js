export default {
    state: {
       district: [],
       address: [],
    },
    //xử lý thao tác chức năng
    getters: {
        getDistrict(state){return state.district},
        gAllAddress(state){return state.address;},
        getShops(state){return state.shops}
    },
    //Diễn tả 1 hành động
    actions: {
        allDictrict(context){
            axios.get('/all-district').then(res=>{context.commit('commitDistrict',res.data)})
        },
        allAdress(context){
            axios.get('/all-address').then((res)=>{context.commit('getaddress',res.data);})
        },
    },
    //Trạng thái không thể thay đổi trực tiếp mà chỉ thay đổi thông qua commit
    //Từ action thay đổi gọi xuống commit của mutations thông qua context.commit
    mutations: {
        commitDistrict(state,data){state.district = data},
        getaddress(state, data){state.address = data },
    }

}
