<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Admin
Route::get('/admin',function(){
    return view('Admin/index');
});

//Adress
Route::get('/all-address','AdressController@index');

//district
Route::get('/all-district','DistrictController@index');

//Shops
Route::get('/all-shops','ShopsController@index')->name('profile');
Route::post('/newshop','ShopsController@store');
Route::get('/editshop/{id}', 'ShopsController@show');


//Path nên dược đặt dưới cùng
Route::any('{path}', 'HomeController@index')->where(['path' => '.*']);